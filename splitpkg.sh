#!/bin/busybox ash 

 # splitpkg: filters DEV, DOC and NLS from packages 

 if [ ! -d "$1" ]; then 

   echo "Usage: splitpkg [DIRECTORY]" 
   echo "Process the package in the directory DIRECTORY to separate DEV, DOC and NLS from it" 
   echo "" 
   echo "Options:" 
   echo "        --help      Display this message and quit" 
   
   case $? in 
   
     --help) 
     
       exit 0 
       ;; 
       
     *) 
     
       exit 1 
       ;; 
       
   esac 
     
 else 

   fullPath=`realpath $1` # the full path to the package 
   packageName=`basename $fullPath` # the package name 
   packageDir=`dirname $fullPath` # the parent directory of the package 

   cd $fullPath 
   
   for i in `find | grep /`; do 
   
     if [ -e $i ]; then 
     
       fileName="`basename $i`" 
     
       case "$fileName" in 
   
         *.la|*.a|*.prl|pkgconfig|include) 
         
           suffix=DEV 
           ;; 
           
         doc|*-doc|help|readme.txt|README.TXT|about.txt|ABOUT.TXT|readme|README) 
     
           suffix=DOC 
           ;; 
       
         man|info) # if it's a directory named "man" or "info", move to DOC 
     
           [ -d $i ] && suffix=DOC 
           ;; 
                   
         locale) # if it's a directory named "locale", move to NLS 
     
           [ -d $i ] && suffix=NLS 
           ;; 
     
         i18n) 
     
           suffix=NLS 
           ;; 
             
       esac 
   
     fi 
   
     case "$suffix" in 
   
       NLS|DOC|DEV) 

         # detect the directory 
         subDir=${i%/$fileName} 
         subDir=${subDir:2} 
         
         case $packageName in 
         
           *-*) 
           
             # add the suffix to the package name 
             subPackageName=${packageName/-/_$suffix-} 
             ;; 
             
           *) 
           
             subPackageName=${packageName}_$suffix 
             ;; 
             
         esac 
         
         # create the directory under the sub-package directory 
         mkdir -p $packageDir/$subPackageName/$subDir 
         
         # move to the sub-package 
         mv $i $packageDir/$subPackageName/$subDir 
               
         # get rid of empty directories in the main package 
         rmdir $packageDir/$packageName/$subDir > /dev/null 2>&1 
         
         unset suffix 
         ;; 
         
     esac 
               
   done 

   exit 0 

 fi